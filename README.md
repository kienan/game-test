# Building

  1. Get the source code:
    `git clone --recursive https://gitea.burntworld.ca/kienan/game-test.git`
  2. Install build dependencies:
    `sudo apt install build-essential cmake freeglut3-dev libgl-dev libglew-dev libglfw3-dev libglm-dev libglx-dev libopengl-dev`
  3. Create the makefiles:
    `mkdir -p game-test/build && cd game-test/build && cmake ..`
  4. Build:
    `make`

# Running the client

  `./client_imgui`
