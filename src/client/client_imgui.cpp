#include <chrono>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include "imgui.h"
#include "imgui_impl_glfw.h"
#include "imgui_impl_opengl3.h"
#include "ui/console.h"

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <glm/gtc/matrix_transform.hpp>
#include "common/logging.hpp"
#include "common/messaging/bus.hpp"

#include "common/shaderloader.hpp"

#include "renderable.hpp"

// Globals
static Logger logger;
static MessageBus messager(&logger);

// End globals

static void error_callback(int error, const char* description)
{
   fprintf(stderr, "Error %d: %s\n", error, description);
}

static void debug_messagebus_callback(Message *m) {
   logger.log(info, "(debug_messagebus_callback) Processed message: '%s'", m->name);
}

int main(int argc, char** argv)
{
    GLFWwindow* window;

    /* Initialize the library */
    glfwSetErrorCallback(error_callback);
    if (!glfwInit()) {
       fprintf(stdout, "[GFLW] Error: failed to init!\n");
       return -1;
    }

    // Create a windowed mode window and its OpenGL context
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
    // Note: the decoration doesn't always work in Wayland, as it depends on the
    // host system implementing wp_viewporter. This should be available in
    // gnome 3.32+; kwin 5.15+
    glfwWindowHint(GLFW_DECORATED, GLFW_TRUE);
    window = glfwCreateWindow(1024, 768, "Client", NULL, NULL);
    if (!window) {
        glfwTerminate();
        return -1;
    }
    glfwMakeContextCurrent(window);
    glfwSwapInterval(1); // Vsync

    if (!gladLoadGL()) {
       fprintf(stderr, "[GLAD] Error loading\n");
       return 1;
    }

    // Setup Dear ImGui context
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO& io = ImGui::GetIO(); (void)io;
    //io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;     // Enable Keyboard Controls
    //io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;      // Enable Gamepad Controls

    // Setup Dear ImGui style
    ImGui::StyleColorsDark();
    //ImGui::StyleColorsClassic();

    // Setup Platform/Renderer bindings
    ImGui_ImplGlfw_InitForOpenGL(window, true);
    ImGui_ImplOpenGL3_Init();

    int height = 0, width = 0;
    ImVec4 clear_color = ImVec4(0.45f, 0.55f, 0.60f, 1.00f);
    float fov = 45.0;

    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);
    // Required before OpenGL Calls.
    GLuint vertex_array;
    glGenVertexArrays(1, &vertex_array);
    glBindVertexArray(vertex_array);

    Renderable::Object triangle = Renderable::Object(Renderable::triangle_data, sizeof(Renderable::triangle_data));
    Renderable::Object cube = Renderable::Object(Renderable::cube_data, sizeof(Renderable::cube_data), Renderable::cube_color, sizeof(Renderable::cube_color));
    cube.set_position(glm::vec3(0.f, 0.f, -5.f));
    static Console console(&messager);
    bool show_console = false;

    // Shader load information
    bool shaders_loaded = false;
    int num_shaders_loaded = 0;
    int num_shaders_failed = 0;
    int num_shader_files = 0;
    const char* shader_list = "shaders/list.txt";
    static struct ShaderRegistry shader_registry;
    loadShadersFromFile(shader_list, &shader_registry);
    static std::vector<RegisteredShaderProgram> shader_program_registry;
    // @BUG This is a temporary hack. When shader_program_registry is grown, the existing
    // entries lose data.
    shader_program_registry.reserve(10);
    loadShaderProgramsFromFile("shaders/programs.txt", &shader_registry, &shader_program_registry);
    messager.registerCallback(&debug_messagebus_callback);
    int window_height, window_width;
    glm::mat4 projection;
    glm::vec3 camera_position = glm::vec3(4.f, 3.f, 3.f);
    glm::vec3 camera_looking_at = glm::vec3(0.f, 0.f, 0.f);
    glm::mat4 view = glm::lookAt(camera_position, glm::vec3(0,0,0), glm::vec3(0,1,0));
    glm::mat4 model_matrix = glm::mat4(1.0f);
    glm::mat4 model_default = glm::mat4(1.0f);
    // The model matrix should eventually be translation * rotation * scale
    glm::mat4 mvp_matrix;

    // "Global" camera input controls
    double camera_velocity = 1.0;
    glm::vec3 camera_translation;
    // Tick timer
    std::chrono::time_point start = std::chrono::high_resolution_clock::now();
    while(!glfwWindowShouldClose(window)) {
       // Tick timer
       auto dt = ((std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::high_resolution_clock::now() - start)).count()) / 100000000.f;
       start = std::chrono::high_resolution_clock::now();
       // Process input.
       glfwPollEvents();

       // Camera movement?
       double cv_x = 0.f;
       double cv_y = 0.f;
       // This is "kind of" a pan, since it translates the point being looked at by the same value.
       if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS) {
          cv_x += camera_velocity * dt;
       }
       if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS) {
          cv_x -= camera_velocity * dt;
       }
       if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS) {
          cv_y += camera_velocity * dt;
       }
       if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS) {
          cv_y -= camera_velocity * dt;
       }
       camera_translation = glm::vec3(cv_x, cv_y, 0.f);
       auto ncp = glm::translate(glm::mat4(1.0f), camera_translation) * glm::vec4(camera_position[0], camera_position[1], camera_position[2], 1.0f);
       auto ncla = glm::translate(glm::mat4(1.0f), camera_translation) * glm::vec4(camera_looking_at[0], camera_looking_at[1], camera_looking_at[2], 1.0f);
       camera_position = glm::vec3(ncp[0], ncp[1], ncp[2]);
       camera_looking_at = glm::vec3(ncla[0], ncla[1], ncla[2]);

       view = glm::lookAt(camera_position, camera_looking_at, glm::vec3(0,1,0));

       // Process messages
       int messages_treated = messager.processAll();
       if (messages_treated > 0) {
          logger.log(info, "Treated %d messages this tick", messages_treated);
       }

       // @TODO Game tick


       // Clear GL buffers
       glClearColor(clear_color.x, clear_color.y, clear_color.z, clear_color.w);
       glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

       // Render game geometries
       glfwGetWindowSize(window, &window_width, &window_height);
       projection = glm::perspective(glm::radians(fov), (float) window_width / (float) window_height,
                                     0.1f, 100.0f);
       glm::mat4 vp = projection * view;
       // @see https://blog.conan.io/2019/06/26/An-introduction-to-the-Dear-ImGui-library.html
       // 1st attribute buffer : vertices
       GLuint p = find_shader_program_by_name("default", &shader_program_registry);
       triangle.render(vp, p);

       // Draw a cube?
       p = find_shader_program_by_name("color", &shader_program_registry);
       cube.render(vp, p);

       // Start the Dear ImGui frame
       ImGui_ImplOpenGL3_NewFrame();
       ImGui_ImplGlfw_NewFrame();
       ImGui::NewFrame();
       {
          static int counter = 0;

          ImGui::Begin("Misc");                          // Create a window called "Hello, world!" and append into it.
          ImGui::Text("Camera Position: %f, %f, %f", camera_position[0], camera_position[1], camera_position[2]);
          ImGui::Text("Camera Looking At: %f, %f, %f", camera_looking_at[0], camera_looking_at[1], camera_looking_at[2]);
          ImGui::SliderFloat("Field of View (FOV)", &fov, 45.0f, 180.0f);            // Edit 1 float using a slider from 0.0f to 1.0f
          ImGui::ColorEdit3("clear color", (float*)&clear_color); // Edit 3 floats representing a color

          ImGui::Separator();
          ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
          ImGui::End();

          console.Draw("Debug Console", &show_console);
       }

       // Finalise rendering
       ImGui::Render();
       ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

       // Swap buffers
       int display_w, display_h;
       glfwGetFramebufferSize(window, &display_w, &display_h);
       glViewport(0, 0, display_w, display_h);
       glfwSwapBuffers(window);
    }

    // Cleanup
    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplGlfw_Shutdown();
    ImGui::DestroyContext();

    glfwTerminate();
    return 0;
}
