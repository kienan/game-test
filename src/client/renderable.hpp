#ifndef _RENDERABLE_H_
#define _RENDERABLE_H_

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <glm/gtc/matrix_transform.hpp>

namespace Renderable {
   class Object {
   public:
      Object(const GLfloat * vertex_data, int vd_size, const GLfloat * color_data = 0, int cd_size = 0) {
         this->vertex_buffer_data = (GLfloat *) vertex_data;
         this->vertex_buffer_data_size = vd_size;
         this->colour_buffer_data = (GLfloat *) color_data;
         this->colour_buffer_data_size = cd_size;
         this->init();
      }
      void init() {
         if (this->vertex_buffer != 0) {
            // @TODO: Delete OpenGL buffer
         }
         if (this->vertex_buffer_data != 0) {
            glGenBuffers(1, &this->vertex_buffer);
            glBindBuffer(GL_ARRAY_BUFFER, this->vertex_buffer);
            glBufferData(GL_ARRAY_BUFFER, this->vertex_buffer_data_size,
                         this->vertex_buffer_data, GL_STATIC_DRAW);
            printf("Vertex_buffer: %d (%d, %d)\n", this->vertex_buffer, this->vertex_buffer_data_size,
                   this->vertex_buffer_data_size / sizeof(GLfloat) / 3);
         }
         if (this->colour_buffer != 0) {
            // @TODO: Delete OpenGL buffer
         }
         if (this->colour_buffer_data != 0) {
            glGenBuffers(1, &this->colour_buffer);
            glBindBuffer(GL_ARRAY_BUFFER, this->colour_buffer);
            glBufferData(GL_ARRAY_BUFFER, this->colour_buffer_data_size,
                         this->colour_buffer_data, GL_STATIC_DRAW);
         }
      }
      void render(glm::mat4 vp, GLuint shader_program = 0) {
         if (this->vertex_buffer == 0) {
            return;
         }
         glUseProgram(shader_program);
         glm::mat4 mvp = vp * this->get_model_matrix();
         GLuint matrix_id = glGetUniformLocation(shader_program, "MVP");
         glUniformMatrix4fv(matrix_id, 1, GL_FALSE, &mvp[0][0]);
         glEnableVertexAttribArray(0);
         glBindBuffer(GL_ARRAY_BUFFER, this->vertex_buffer);
         glVertexAttribPointer(
            0,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
            3,                  // size
            GL_FLOAT,           // type
            GL_FALSE,           // normalized?
            0,                  // stride
            (void*)0            // array buffer offset
            );
         if (this->colour_buffer == 0) {
            glDrawArrays(GL_TRIANGLES, 0, this->vertex_buffer_data_size / sizeof(GLfloat) / 3); // Starting from vertex 0; 3 vertices total -> 1 triangle
            glDisableVertexAttribArray(0);
            glUseProgram(0);
            return;
         }
         glEnableVertexAttribArray(1);
         glBindBuffer(GL_ARRAY_BUFFER, this->colour_buffer);
         glVertexAttribPointer(
            1,                                // attribute. No particular reason for 1, but must match the layout in the shader.
            3,                                // size
            GL_FLOAT,                         // type
            GL_FALSE,                         // normalized?
            0,                                // stride
            (void*)0                          // array buffer offset
            );
         glDrawArrays(GL_TRIANGLES, 0, this->vertex_buffer_data_size / sizeof(GLfloat) / 3); // Starting from vertex 0; 3 vertices total -> 1 triangle
         glDisableVertexAttribArray(0);
         glDisableVertexAttribArray(1);
         glUseProgram(0);
      }
      void set_position(glm::vec3 position) {
         this->translation = glm::translate(glm::mat4(1.f), position);
      }
   private:
      glm::mat4 get_model_matrix() {
         return translation * rotation * scale;
      }
      GLuint vertex_buffer = 0;
      GLuint colour_buffer = 0;
      GLfloat *colour_buffer_data = 0;
      int colour_buffer_data_size = 0;
      GLfloat *vertex_buffer_data = 0;
      int vertex_buffer_data_size = 0;
      glm::mat4 translation = glm::mat4(1.f);
      glm::mat4 scale = glm::mat4(1.f);
      glm::mat4 rotation = glm::mat4(1.f);
   };

   static const GLfloat cube_data[] = {
      -1.0f,-1.0f,-1.0f, // triangle 1 : begin
      -1.0f,-1.0f, 1.0f,
      -1.0f, 1.0f, 1.0f, // triangle 1 : end
      1.0f, 1.0f,-1.0f, // triangle 2 : begin
      -1.0f,-1.0f,-1.0f,
      -1.0f, 1.0f,-1.0f, // triangle 2 : end
      1.0f,-1.0f, 1.0f,
      -1.0f,-1.0f,-1.0f,
      1.0f,-1.0f,-1.0f,
      1.0f, 1.0f,-1.0f,
      1.0f,-1.0f,-1.0f,
      -1.0f,-1.0f,-1.0f,
      -1.0f,-1.0f,-1.0f,
      -1.0f, 1.0f, 1.0f,
      -1.0f, 1.0f,-1.0f,
      1.0f,-1.0f, 1.0f,
      -1.0f,-1.0f, 1.0f,
      -1.0f,-1.0f,-1.0f,
      -1.0f, 1.0f, 1.0f,
      -1.0f,-1.0f, 1.0f,
      1.0f,-1.0f, 1.0f,
      1.0f, 1.0f, 1.0f,
      1.0f,-1.0f,-1.0f,
      1.0f, 1.0f,-1.0f,
      1.0f,-1.0f,-1.0f,
      1.0f, 1.0f, 1.0f,
      1.0f,-1.0f, 1.0f,
      1.0f, 1.0f, 1.0f,
      1.0f, 1.0f,-1.0f,
      -1.0f, 1.0f,-1.0f,
      1.0f, 1.0f, 1.0f,
      -1.0f, 1.0f,-1.0f,
      -1.0f, 1.0f, 1.0f,
      1.0f, 1.0f, 1.0f,
      -1.0f, 1.0f, 1.0f,
      1.0f,-1.0f, 1.0f
   };
   static const GLfloat cube_color[] = {
      0.583f,  0.771f,  0.014f,
      0.609f,  0.115f,  0.436f,
      0.327f,  0.483f,  0.844f,
      0.822f,  0.569f,  0.201f,
      0.435f,  0.602f,  0.223f,
      0.310f,  0.747f,  0.185f,
      0.597f,  0.770f,  0.761f,
      0.559f,  0.436f,  0.730f,
      0.359f,  0.583f,  0.152f,
      0.483f,  0.596f,  0.789f,
      0.559f,  0.861f,  0.639f,
      0.195f,  0.548f,  0.859f,
      0.014f,  0.184f,  0.576f,
      0.771f,  0.328f,  0.970f,
      0.406f,  0.615f,  0.116f,
      0.676f,  0.977f,  0.133f,
      0.971f,  0.572f,  0.833f,
      0.140f,  0.616f,  0.489f,
      0.997f,  0.513f,  0.064f,
      0.945f,  0.719f,  0.592f,
      0.543f,  0.021f,  0.978f,
      0.279f,  0.317f,  0.505f,
      0.167f,  0.620f,  0.077f,
      0.347f,  0.857f,  0.137f,
      0.055f,  0.953f,  0.042f,
      0.714f,  0.505f,  0.345f,
      0.783f,  0.290f,  0.734f,
      0.722f,  0.645f,  0.174f,
      0.302f,  0.455f,  0.848f,
      0.225f,  0.587f,  0.040f,
      0.517f,  0.713f,  0.338f,
      0.053f,  0.959f,  0.120f,
      0.393f,  0.621f,  0.362f,
      0.673f,  0.211f,  0.457f,
      0.820f,  0.883f,  0.371f,
      0.982f,  0.099f,  0.879f
   };

   static const GLfloat triangle_data[] = {
       -1.0f, -1.0f, 0.0f,
       1.0f, -1.0f, 0.0f,
       0.0f,  1.0f, 0.0f,
    };
}

#endif // _RENDERABLE_H_
