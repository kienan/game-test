#include <math.h>
#include <stdio.h>
#include <string.h>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

// Nuklear
#define NK_INCLUDE_FIXED_TYPES
#define NK_INCLUDE_STANDARD_IO
#define NK_INCLUDE_STANDARD_VARARGS
#define NK_INCLUDE_DEFAULT_ALLOCATOR
#define NK_INCLUDE_VERTEX_BUFFER_OUTPUT
#define NK_INCLUDE_FONT_BAKING
#define NK_INCLUDE_DEFAULT_FONT
#define NK_IMPLEMENTATION
#define NK_GLFW_GL3_IMPLEMENTATION
#define NK_KEYSTATE_BASED_INPUT
#include <nuklear.h>
#include "nuklear/nuklear_glfw_gl3.h"

#define MAX_VERTEX_BUFFER 512 * 1024
#define MAX_ELEMENT_BUFFER 128 * 1024

static void error_callback(int error, const char* description)
{
   fprintf(stderr, "Error %d: %s\n", error, description);
}

int main(int argc, char** argv)
{
    GLFWwindow* window;

    /* Initialize the library */
    glfwSetErrorCallback(error_callback);
    if (!glfwInit()) {
       fprintf(stdout, "[GFLW] Error: failed to init!\n");
       return -1;
    }

    /* Create a windowed mode window and its OpenGL context */
    window = glfwCreateWindow(1024, 768, "Client", NULL, NULL);
    glfwSetWindowAttrib(window, GLFW_DECORATED, GLFW_TRUE);
    if (!window) {
        glfwTerminate();
        return -1;
    }
    glfwMakeContextCurrent(window);
    glfwSwapInterval(1); // Vsync

    if (!gladLoadGL()) {
       fprintf(stderr, "[GLAD] Error loading\n");
       return 1;
    }

    int width = 0, height = 0;
    struct nk_context *ctx;
    ctx = nk_glfw3_init(window, NK_GLFW3_INSTALL_CALLBACKS);
    {
       struct nk_font_atlas *atlas;
       nk_glfw3_font_stash_begin(&atlas);
       nk_glfw3_font_stash_end();
    }
       struct nk_colorf bg;
    bg.r = 0.10f, bg.g = 0.18f, bg.b = 0.24f, bg.a = 1.0f;
    double previousTime = glfwGetTime();
    int frameCount = 0;
    while(!glfwWindowShouldClose(window)) {
       glfwPollEvents();
       // Process input.
       nk_glfw3_new_frame();
       if (nk_begin(ctx, "Demo", nk_rect(50, 50, 230, 250),
                    NK_WINDOW_BORDER|NK_WINDOW_MOVABLE|NK_WINDOW_SCALABLE|
                    NK_WINDOW_MINIMIZABLE|NK_WINDOW_TITLE))
       {
          enum {EASY, HARD};
          static int op = EASY;
          static int property = 20;
          nk_layout_row_static(ctx, 30, 80, 1);
          if (nk_button_label(ctx, "button"))
             fprintf(stdout, "button pressed\n");

          nk_layout_row_dynamic(ctx, 30, 2);
          if (nk_option_label(ctx, "easy", op == EASY)) op = EASY;
          if (nk_option_label(ctx, "hard", op == HARD)) op = HARD;

          nk_layout_row_dynamic(ctx, 25, 1);
          nk_property_int(ctx, "Compression:", 0, &property, 100, 10, 1);

          nk_layout_row_dynamic(ctx, 20, 1);
          nk_label(ctx, "background:", NK_TEXT_LEFT);
          nk_layout_row_dynamic(ctx, 25, 1);
          if (nk_combo_begin_color(ctx, nk_rgb_cf(bg), nk_vec2(nk_widget_width(ctx),400))) {
             nk_layout_row_dynamic(ctx, 120, 1);
             bg = nk_color_picker(ctx, bg, NK_RGBA);
             nk_layout_row_dynamic(ctx, 25, 1);
             bg.r = nk_propertyf(ctx, "#R:", 0, bg.r, 1.0f, 0.01f,0.005f);
             bg.g = nk_propertyf(ctx, "#G:", 0, bg.g, 1.0f, 0.01f,0.005f);
             bg.b = nk_propertyf(ctx, "#B:", 0, bg.b, 1.0f, 0.01f,0.005f);
             bg.a = nk_propertyf(ctx, "#A:", 0, bg.a, 1.0f, 0.01f,0.005f);
             nk_combo_end(ctx);
          }
          double currentTime = glfwGetTime();
          char buffer[50];
          sprintf(buffer, "Framerate: %f FPS", frameCount / (currentTime - previousTime));
          if (currentTime - previousTime >= 1.0) {
             frameCount = 0;
             previousTime = currentTime;
          }
          frameCount++;
          nk_label(ctx, &buffer[0], NK_TEXT_LEFT);
       }
       nk_end(ctx);

       // @TODO Game tick

       // Rendering
       /* IMPORTANT: `nk_glfw_render` modifies some global OpenGL state
        * with blending, scissor, face culling, depth test and viewport and
        * defaults everything back into a default state.
        * Make sure to either a.) save and restore or b.) reset your own state after
        * rendering the UI. */
       glfwGetWindowSize(window, &width, &height);
       glViewport(0, 0, width, height);
       glClear(GL_COLOR_BUFFER_BIT);
       glClearColor(bg.r, bg.g, bg.b, bg.a);
       nk_glfw3_render(NK_ANTI_ALIASING_ON, MAX_VERTEX_BUFFER, MAX_ELEMENT_BUFFER);
       glfwSwapBuffers(window);
    }

    nk_glfw3_shutdown();
    glfwTerminate();
    return 0;
}
