#ifndef _MESSAGE_BUS_H
#define _MESSAGE_BUS_H

#include <assert.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <vector>

#include "common/logging.hpp"

struct MessageParam {
   public:
   int data_size;
   void *data;
   MessageParam(void *src, int size) {
      data_size = size;
      data = src;
   }
};

struct Message {
   const char *name;
   int parameter_count;
   Message(const char *name) {
      this->name = name;
   }
};

class MessageBus {
   // arrray of things
   // This class is responsible for allocating and freeing memory for the
   // messages and their parameters.
   private:
   Message *bufferStart;
   int bufferSize;
   Message *occupiedHead;
   Message *occupiedTail;
   int count;
   Logger *logger;
   std::vector<void (*)(Message*)> callbacks;

   public:
   MessageBus(Logger *l, int initial_size = 100) {
      this->bufferSize = initial_size;
      this->bufferStart = (Message *) calloc(initial_size, sizeof(struct Message));
      this->occupiedHead = NULL; // the furthest "into" the buffer we are
      this->occupiedTail = NULL; // the closest to the "start" of the buffer the useful bits are
      this->count = 0;
      this->logger = l;
      l->log(info, "Initialized MessageBus with a buffer of %d items (%d bytes), starting at %d",
             initial_size, initial_size*sizeof(struct Message), this->bufferStart);
   }
  ~MessageBus() {
      free(this->bufferStart);
   }

   void registerCallback(void (*fp)(Message*)) {
      this->callbacks.push_back(fp);
   }

   void removeCallback(void (*fp)(Message*)) {
      bool found = false;
      int i;
      for(i = 0 ; i < this->callbacks.size(); i++) {
         if (fp == this->callbacks.at(i)) {
            found = true;
            break;
         }
      }
      if (found) {
         this->callbacks.erase(this->callbacks.begin() + i);
      }
   }

   void add(const char *name) {
      Message *new_item;
      if (occupiedHead == NULL) {
         new_item = this->bufferStart;
         this->occupiedHead = this->bufferStart;
         this->occupiedTail = this->bufferStart;
         this->count++;
         this->logger->log(info, "Added new item at start of empty buffer");
      }
      else {
         // Do we have free space?
         if (count < bufferSize) {
            new_item = this->occupiedHead + sizeof(struct Message);
            this->occupiedHead += sizeof(struct Message);
            this->count++;
            this->logger->log(info, "Added new item in buffer with free space");
         }
         else {
            // Free up space (realloc, grow if necessary);
            this->realloc();
            new_item = this->occupiedHead + sizeof(struct Message);
            this->occupiedHead += sizeof(struct Message);
            this->count++;
            this->logger->log(info, "Add new item after re-allocation");
         }
      }
      new_item->name = name;
      this->logger->log(info, "%d items in MessageBus buffer", this->count);
   }

   void remove() {
      this->occupiedTail += sizeof(struct Message);
      this->count--;
   }

   void realloc() {
      // How much free space do we have at the start?
      int start_available = (this->occupiedTail - this->bufferStart) / sizeof(struct Message);
      // We'll do a full grow if there's less than 25% free
      int newsize = this->bufferSize;
      this->logger->log(info, "Re-allocating MessageBus buffer to gain some space");
      if (start_available < this->bufferSize / 25) {
         int newsize = this->bufferSize * 2;
         this->logger->log(info, "Growing message buffer from %d to %d", this->bufferSize, newsize);
      }
      Message *newStart = (Message *) calloc(newsize, sizeof(struct Message));
      int copySize = this->occupiedHead + sizeof(struct Message) - this->occupiedTail;
      memcpy(newStart, this->occupiedTail, copySize);
      Message *newHead;
      newHead = newStart + (this->occupiedHead - this->occupiedTail);
      this->bufferStart = newStart;
      this->occupiedTail = newStart;
      this->occupiedHead = newHead;
   }

   int processAll() {
      int done = 0;
      while(this->count > 0) {
         this->process();
         done++;
      }
      return done;
   }

   void process() {
      assert(this->count > 0);
      // Call all the listening entities that care about this message or whatever
      for (int i = 0 ; i < this->callbacks.size() ; i++) {
         this->callbacks.at(i)(this->occupiedTail);
      }
      this->remove();
   }
};

#endif
