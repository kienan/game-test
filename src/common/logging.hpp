#ifndef _LOGGING_H_
#define _LOGGING_H_

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

enum LogLevel {
   vvvdebug,
   debug,
   info,
   warning,
   error
};

class Logger {

   LogLevel currentLevel;
   FILE **fds;
   int fd_count = 0;
   public:
   Logger(LogLevel level = info) {
      this->currentLevel = level;
      this->attachFd(stderr);
   }

   void attachFd(FILE *fd) {
      int newSize = this->fd_count + 1;
      FILE **newfds = (FILE **)calloc(newSize, sizeof(FILE*));
      if (this->fds != NULL) {
         memcpy(newfds, this->fds,this->fd_count * sizeof(FILE*));
         free(this->fds);
         this->fds = newfds;
      }
      else {
         this->fds = newfds;
      }
      this->fds[newSize-1] = fd;
      this->fd_count = newSize;
   }

   void removeFd(FILE *fd) {
      int newSize = this->fd_count - 1;
      if (newSize <= 0) {
         free(this->fds);
         this->fds = NULL;
         this->fd_count = 0;
         return;
      }
      FILE **newfds = (FILE **) calloc(newSize, sizeof(FILE*));
      int new_i = 0;
      for (int i = 0 ; i < this->fd_count ; i++) {
         if (fd == this->fds[i]) {
            continue;
         } else {
            newfds[new_i] = this->fds[i];
            new_i++;
         }
      }
      free(this->fds);
      this->fds = newfds;
      this->fd_count = newSize;
   }

   const char* mapLevel(LogLevel l) {
      switch (l) {
      case vvvdebug:
         return "vvvdebug";
         break;
      case debug:
         return "debug";
         break;
      case info:
         return "info";
         break;
      case warning:
         return "warning";
         break;
      case error:
         return "error";
         break;
      default:
         return "unknown";
         break;
      }
   }

   void setLevel(LogLevel l) {
      this->currentLevel = l;
   }

   void log(LogLevel level, char* str) {
      this->log(level, "%s", str);
   }

   void log(LogLevel level, const char *fmt, ...) {
      if (level < this->currentLevel) {
         return;
      }
      if (this->fd_count <= 0) {
         // No attached fds.
         return;
      }
      char buf[1024];
      memset(buf, 0, 1024*sizeof(char));
      va_list args;
      va_start(args, fmt);
      vsnprintf(buf, 1024, fmt, args);
      for (int i = 0; i < this->fd_count ; i++) {
         fprintf(this->fds[i], "[%s]\t%s\n", this->mapLevel(level), buf);
         fflush(this->fds[i]);
      }
      va_end(args);
   }
};
#endif // _LOGGING_H_
