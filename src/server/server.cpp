#include <stdio.h>
#include <string.h>

#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>

#define SERVER_UDP_PORT 2800
#define UDP_PACKET_SIZE 500
#define UDP_BUFFER_SIZE 10000

int main(int argc, char** argv) {

   int fd;
   if ((fd = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
      perror("cannot create socket");
      return -1;
   }

   struct sockaddr_in sin;
   memset(&sin, 0, sizeof(sin));
   sin.sin_family = AF_INET;
   sin.sin_addr.s_addr = htonl(INADDR_ANY);
   sin.sin_port = htons(SERVER_UDP_PORT);

   if (bind(fd, (struct sockaddr *) &sin, sizeof(sin)) < 0) {
      perror("bind failed");
      return -1;
   }

   unsigned char buf[UDP_PACKET_SIZE];
   int recvlen;
   struct sockaddr_in remote_address;
   socklen_t addrlen = sizeof(remote_address);
   for (;;) {
      printf("waiting on port %d\n", SERVER_UDP_PORT);
      recvlen = recvfrom(fd, buf, UDP_PACKET_SIZE, 0,
                         (struct sockaddr *) &remote_address,
                         &addrlen);
      if (recvlen > 0) {
         buf[recvlen] = 0;
         printf("received message: \"%s\"\n", buf);
      }
   }
}
