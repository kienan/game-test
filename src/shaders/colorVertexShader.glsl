#version 300 es
layout(location = 0) in vec3 vertexPosition_modelspace;
layout(location = 1) in vec3 vertexColor;
uniform mat4 MVP;
out mediump vec3 fragmentColor;
void main(){
  gl_Position.xyz = vertexPosition_modelspace;
  gl_Position.w = 1.0;
  gl_Position = MVP * gl_Position;
  fragmentColor = vertexColor;
}
