#version 300 es
// the precision mediump is required in 3.0es
in mediump vec3 fragmentColor;
out mediump vec3 color;
void main(){
  color = fragmentColor;
}
